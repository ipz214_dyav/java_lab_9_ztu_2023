package com.education.ua;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

public class Task3 {
    public static void main(String[] args) throws ClassNotFoundException, InstantiationException, IllegalAccessException {
        Class<?> class1 = CustomClass.class;

        CustomClass instance = new CustomClass();
        Class<?> class2 = instance.getClass();

        Class<?> class3 = Class.forName("com.education.ua.CustomClass");

        Field[] fields = class1.getDeclaredFields();
        System.out.println("���� �����:");
        for (Field field:fields) {
            System.out.println(field.getName());
        }

        Constructor<?>[] constructors = class2.getDeclaredConstructors();
        System.out.println("\n������������ �����:");
        for (Constructor<?> construct:constructors) {
            System.out.println(construct.getName());
        }

        Method[] methods = class3.getDeclaredMethods();
        System.out.println("\n������ �����:");
        for (Method method : methods) {
            System.out.println(method.getName());
        }


        CustomClass customClass = (CustomClass) class1.newInstance();

        customClass.publicMethodWithArgs(12,"text");

        customClass.setPrivateField(142);
        System.out.println("�������� ����: " + customClass.getPrivateField());

    }
}
