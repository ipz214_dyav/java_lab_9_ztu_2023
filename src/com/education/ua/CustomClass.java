package com.education.ua;

public class CustomClass {
    // �������� ���� �����
    private int privateField;
    private String anotherPrivateField;

    // ����������� ��� ���������
    public CustomClass() {
        // ������������ �� �������������
        privateField = 0;
        anotherPrivateField = "default";
    }

    // ����������� � �����������
    public CustomClass(int privateField, String anotherPrivateField) {
        // ������������ ���� � ���������� ����������
        this.privateField = privateField;
        this.anotherPrivateField = anotherPrivateField;
    }

    // �������� ����� ��� ���������
    public void publicMethodWithoutArgs() {
        System.out.println("�� �������� ����� ��� ���������");
    }

    // �������� ����� � �����������
    public void publicMethodWithArgs(int number, String text) {
        System.out.println("�� �������� ����� � �����������: " + number + ", " + text);
    }

    // ������ ��� ��������� �������� ���������� ���� privateField
    public int getPrivateField() {
        return privateField;
    }

    // ������ ��� ������������ �������� ���������� ���� privateField
    public void setPrivateField(int privateField) {
        this.privateField = privateField;
    }

    // ������ ��� ��������� �������� ���������� ���� anotherPrivateField
    public String getAnotherPrivateField() {
        return anotherPrivateField;
    }

    // ������ ��� ������������ �������� ���������� ���� anotherPrivateField
    public void setAnotherPrivateField(String anotherPrivateField) {
        this.anotherPrivateField = anotherPrivateField;
    }
}
