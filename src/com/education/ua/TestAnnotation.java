package com.education.ua;

import java.lang.reflect.Method;
import java.util.Arrays;

public class TestAnnotation {

    @CodeReviewAnnotation(
            reviewer = "JohnDoe",
            comments = {"Optimize this algorithm", "Improve variable naming"},
            status = "In progress"
    )
    public void sortArr(int[] array) {
        Arrays.sort(array);
        System.out.println("Elements of array sorted in ascending order: ");
        for (int j : array) {
            System.out.println(j);
        }
    }

    public static void main(String[] args) throws NoSuchMethodException {
        Class<?> class1 = TestAnnotation.class;
        Method method = class1.getMethod("sortArr", int[].class);

        CodeReviewAnnotation annotation = method.getAnnotation(CodeReviewAnnotation.class);
        if(annotation != null){
            String reviewer = annotation.reviewer();
            String[] comments = annotation.comments();
            String status = annotation.status();
            boolean approved = annotation.approved();

            System.out.println("Reviewer: " + reviewer);
            System.out.println("Comments:");
            for (String comment : comments) {
                System.out.println("- " + comment);
            }
            System.out.println("Status: " + status);
            System.out.println("Approved: " + approved);
        }
    }
}
